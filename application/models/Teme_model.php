<?php

          defined('BASEPATH') OR exit('No direct script access allowed');
          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Teme_model
           *
           * @author Habib Koe Tkj
           */
          class Teme_model extends CI_Model {
                    /*
                     * fungsi lek bawak sine ye beit dete lengan database lamun dengan ken teme aning sistem
                     * jeri fungsi lek bawak sine si jeri nyocokin dete user, antara input lek form kence lek database
                     */
                    public function teme_proses() {
                              $u_aran_user = set_value('u_aran_user');
                              $u_password = set_value('u_password');

                              $result = $this->db->where('u_aran_user', $u_aran_user)
                                        ->where('u_password', md5($u_password))
                                        ->limit(1)
                                        ->get('user');
                              if ($result->num_rows() > 0) {
                                        return $result->row();
                              } else {
                                        return array();
                              }
                    }
                    /*
                     * fungsi lek bawak sine, ye si jeri penghubung input data aning database
                     */
                    public function daftar() {
                              /*
                               * Cek apakah umur user di bawah 20 tahun
                               * lamun lek bawak 20 tahun je berarti levelnya otomatis di seting siswa
                               * laguk lamun lek atas 20 tahun je otomatis te set ye jeri guru
                               */
                              $u_level_cek = date('Y') - $this->input->post('u_tahun_lahir');
                              if($u_level_cek < 20){
                                        $u_level = 'siswa';
                              }else{
                                        $u_level = 'guru';
                              }
                              $data = array(
                                        'u_aran_lengkap' => $this->input->post('u_aran_lengkap'),
                                        'u_aran_user' => $this->input->post('u_aran_user'),
                                        'u_password' => md5($this->input->post('u_password')),
                                        'u_tanggal_lahir' => $this->input->post('u_tanggal_lahir'),
                                        'u_bulan_lahir' => $this->input->post('u_bulan_lahir'),
                                        'u_tahun_lahir' => $this->input->post('u_tahun_lahir'),
                                        'u_level' => $u_level
                              );
                              $query = $this->db->insert('user', $data);
                              if($query){
                                        return TRUE;
                              }else{
                                        return FALSE;
                              }
                    }

          }
          