<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Dakwah_model
           * 
           * Dakwah model te manfaatan jeri ngatur data dakwah lengan database
           * romboq = insert
           * beit = select
           * kremaq = update
           * sedaq delete
           * beit_sbiji = select where
           * 
           * @author Habib Koe Tkj
           */
          class Dakwah_model extends CI_Model{
                    
                    public function __construct() {
                              parent::__construct();
                    }
                    
                    /*
                     * fungsi lek bawak sine ye kedunte romboq data aning database
                     */
                    public function romboq($u_id) {
                              
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('d_judul'));
                              $data = array(
                                             'd_judul' => $this->input->post('d_judul'),
                                             'd_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'd_isi' => $this->input->post('d_isi'),
                                             'd_jenis' => $this->input->post('d_jenis'),
                                             'id_u' => $u_id
                              );
                              $this->db->insert('dakwah', $data);
                    }
                    
                    /*
                     * fungsi lek bawak sine ye si manfaatan te jeri kremaq data lamun te salak ketik
                     */
                    public function kremaq($u_id, $id) {
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('d_judul'));
                              $data = array(
                                             'd_judul' => $this->input->post('d_judul'),
                                             'd_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'd_isi' => $this->input->post('d_isi'),
                                             'd_jenis' => $this->input->post('d_jenis'),
                                             'id_u' => $u_id
                              );
                              $this->db->where('d_id', $id)
                                        ->update('dakwah', $data);
                    }
                    
                    /*
                     * beit selapuk data dakwah lengan database
                     */
                    public function beit() {
                              $query = $this->db->order_by('d_id', 'desc')
                                                            ->get('dakwah');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }

                    /*
                     * sedaq data sesuai si melente
                     */
                    public function sedaq($id) {
                              $this->db->where('d_id', $id)
                                             ->delete('dakwah');
                    }
                    
                    /*
                     * beit skek file lengan database
                     */
                    public function beit_sbiji($id) {
                              $query = $this->db->where('d_id', $id)
                                                            ->limit(1)
                                                            ->get('dakwah');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              } else {
                                        return array();
                              }
                    }
                    
                    /*
                     * beit data sesuai alamat url si butuh te
                     */
                    public function beit_kedu_url($url) {
                              $query = $this->db->where('d_url', $url)
                                                            ->limit(1)
                                                            ->get('dakwah');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              } else {
                                        return array();
                              }
                    }
                    
                    /*
                     * beit data sesuai user id
                     */
                    public function beit_kedu_user($u_id) {
                              $query = $this->db->where('id_u', $u_id)
                                                            ->get('dakwah');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }
          }
          