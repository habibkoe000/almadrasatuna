<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Halaman_model
           * 
            * Halaman model te manfaatan jeri ngatur data halaman lengan database
           * romboq = insert
           * beit = select
           * kremaq = update
           * sedaq delete
           * beit_sbiji = select where
           * 
           * @author Habib Koe Tkj
           */
          class Halaman_model extends CI_Model{
                    
                    public function __construct() {
                              parent::__construct();
                    }
                    /*
                     * romboq data halaman 
                     */
                    public function romboq($u_id) {
                              
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('h_judul'));
                              $data = array(
                                             'h_judul' => $this->input->post('h_judul'),
                                             'h_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'h_isi' => $this->input->post('h_isi'),
                                             'h_jenis' => $this->input->post('h_jenis'),
                                             'id_u' => $u_id
                              );
                              $this->db->insert('halaman', $data);
                    }
                    
                    /*
                     * kremaq atau edit data halaman
                     */
                    public function kremaq($u_id, $id) {
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('h_judul'));
                              $data = array(
                                             'h_judul' => $this->input->post('h_judul'),
                                             'h_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'h_isi' => $this->input->post('h_isi'),
                                             'h_jenis' => $this->input->post('h_jenis'),
                                             'id_u' => $u_id
                              );
                              $this->db->where('h_id', $id)
                                        ->update('halaman', $data);
                    }
                    
                    /*
                     * beit selapuk data halaman
                     */
                    public function beit() {
                              $query = $this->db->order_by('h_id', 'desc')
                                        ->get('halaman');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }
                    
                    /*
                     * sedaq atau hapus data halaman sesuai id
                     */
                    public function sedaq($id) {
                              $this->db->where('h_id', $id)
                                        ->delete('halaman');
                    }
                    
                    /*
                     * beit skek data sesuai id dete sino
                     */
                    public function beit_sbiji($id) {
                              $query = $this->db->where('h_id', $id)
                                        ->limit(1)
                                        ->get('halaman');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              } else {
                                        return array();
                              }
                    }
                    
                    /*
                     * beit dete halaman menurut alamat url
                     */
                    public function beit_kedu_url($url) {
                              $query = $this->db->where('h_url', $url)
                                        ->limit(1)
                                        ->get('halaman');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              } else {
                                        return array();
                              }
                    }
          }
          