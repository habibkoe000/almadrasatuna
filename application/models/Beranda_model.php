<?php
defined('BASEPATH') OR exit('No direct script access allowed');
          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Beranda_model
           * 
           * Beranda model di gunakan untuk menyediakan data yang berhubungan halaman utama website
           * romboq = insert
           * beit = select
           * kremaq = update
           * sedaq delete
           * beit_sbiji = select where
           * 
           * @author Habib Koe Tkj
           */
          class Beranda_model extends CI_Model{
                    
                    public function __construct() {
                              parent::__construct();
                    }
                    
                    /*
                     * fungsi lek bawak sine te kedu lamun te ken beit data lek tabel database halaman
                     * sesuai jenis si te butuhan
                     *
                     */
                    public function beit_halaman_kedu_jenis($jenis) {
                              $query = $this->db->where('h_jenis', $jenis)
                                        ->get('halaman');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }
                    /*
                     * fungsi lek bawak sine, ye si berfungsi beit dakwah menurut jenis atau kategori si tebutuhan
                     */
                    public function beit_dakwah_kedu_jenis($jenis, $limit) {
                              $query = $this->db->where('d_jenis', $jenis)
                                                            ->limit($limit)
                                                            ->get('dakwah');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }
          }
          