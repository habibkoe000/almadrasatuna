<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of User_model
           * 
           * User model te manfaatan jeri ngatur data user lengan database
           * romboq = insert
           * beit = select
           * kremaq = update
           * sedaq delete
           * beit_sbiji = select where
           * 
           * @author Habib Koe Tkj
           */
          class User_model extends CI_Model {

                    public function __construct() {
                              parent::__construct();
                    }

                    public function romboq($data) {
                              $this->db->insert('user', $data);
                    }
                    
                    /*
                     * fungsi lek bawak sine ye manfaatan te kedu edit data akun te sendiri
                     */
                    public function kremaq($id) {
                              $data = array(
                                             'u_aran_lengkap' => $this->input->post('u_aran_lengkap'),
                                             'u_kelamin' => $this->input->post('u_kelamin'),
                                             'u_email' => $this->input->post('u_email'),
                                             'u_alamat' => $this->input->post('u_alamat'),
                                             'u_telepon' => $this->input->post('u_telepon'),
                                             'u_tanggal_lahir' => $this->input->post('u_tanggal_lahir'),
                                             'u_bulan_lahir' => $this->input->post('u_bulan_lahir'),
                                             'u_tahun_lahir' => $this->input->post('u_tahun_lahir')
                              );
                              $this->db->where('u_id', $id)
                                        ->update('user', $data);
                    }
                    
                    /*
                     * beit data user
                     */
                    public function beit() {
                              $query = $this->db->order_by('u_id', 'desc')
                                        ->get('user');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }
                    
                    /*
                     * sedaq atau hapus user
                     */
                    public function sedaq($id) {
                              $this->db->where('u_id', $id)
                                        ->delete('user');
                    }
                    
                    /*
                     * beit skek data user menurut id user
                     */
                    public function beit_sbiji($id) {
                              $query = $this->db->where('u_id', $id)
                                        ->limit(1)
                                        ->get('user');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              } else {
                                        return array();
                              }
                    }

          }
          