<div class="container out-top-10 main">
          <div class="row">
                    <div class="col s12 nav-breadcumd">
                              <a href="<?= base_url() ?>">Beranda</a> / <a href=""><?= $data_detail->d_jenis ?></a> / <a href=""><?= $data_detail->d_judul ?></a>
                    </div>
          </div>
          <div class="row">
                    <div class="col m9 s12 out-vertical-10">
                              <h4 class="center"><?= $data_detail->d_judul ?></h4>
                              <?= $data_detail->d_isi ?>
                    </div>
                    <div class="col m3 s12">
                              <h5 class="orange-text">Dakwah Lainnya</h5>
                              <ul class="collection">
                                        <?php foreach ($data_dakwah as $value): ?>
                                                  <li class="collection-item avatar">
                                                            <i class="material-icons circle teal">favorite</i>
                                                            <a href="<?= base_url().'dakwah/'.$value->d_url ?>"><span class="title"><?= $value->d_judul ?></span></a>
                                                  </li>
                                        <?php endforeach; ?>
                              </ul>
                    </div>
          </div>
</div>