<div class="container out-top-10 main">
          <div class="row">
                    <div class="col s12 nav-breadcumd">
                              <a href="<?= base_url() ?>">Dashboard</a> / <a href="<?= base_url().'muri/dakwah' ?>">Dakwah</a> / <a href="<?= base_url().'muri/dakwah/romboq' ?>">Siarkan Dakwah</a>
                    </div>
          </div>
          <div class="row">
                    <?= form_open('muri/dakwah/romboq') ?>
                    <div class="col m9 s12 out-vertical-10">
                              <h5><?= $judul ?></h5>
                              
                              <div class="row">
                                        <div class="input-field col s12">
                                                  <input id="d_judul" type="text" name="d_judul" class="validate">
                                                  <label for="d_judul">Judul Dakwah</label>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col s12">
                                                  <textarea id="tinyMce" name="d_isi"></textarea>
                                        </div>
                              </div>
                              

                    </div>
                    <div class="col m3 s12 out-vertical-10">
                              <div class="row">
                                        <div class="col s12">
                                                  <h6 class="orange-text bold">Terbitkan</h6>
                                                  <p>
                                                            Status : Draft <br>
                                                            Visibility : Umum <br>

                                                  </p>
                                                  <button class="btn waves-effect waves-light right" type="submit">Terbitkan
                                                  </button>
                                        </div>
                              </div>
                              <hr>
                              <div class="row">
                                        <div class="col s12">
                                                  <h6 class="orange-text bold">Kategori</h6>
                                                  <p>
                                                            <input name="d_jenis" type="radio" id="d_jenis" value="dakwah" />
                                                            <label for="d_jenis">Dakwah</label>
                                                  </p>
                                                  <p>
                                                            <input name="d_jenis" type="radio" id="d_jenis2" value="kata-mutiara" />
                                                            <label for="d_jenis2">Kata Mutiara</label>
                                                  </p>
                                                  
                                        </div>
                              </div>
                              <hr>
                              <div class="row">
                                        <div class="col s12">
                                                  <h6 class="orange-text">Menu</h6>
                                                  <?php $this->load->view('muri/muri_sidebar'); ?>
                                        </div>
                              </div>
                    </div>
                    <?= form_close() ?>
          </div>
</div>

