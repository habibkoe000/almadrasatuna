<div class="container out-top-10 main">
          <div class="row">
                    <div class="col s12 nav-breadcumd">
                              <a href="<?= base_url() ?>">Dashboard</a> / <a href="<?= base_url().'muri/dakwah' ?>">Dakwah</a>
                    </div>
          </div>
          <div class="row">
                    <div class="col m9 s12 out-vertical-10">
                              <h5><?= $judul ?></h3>
                                        <table id="example1" class="table table-bordered table-striped">
                                                  <thead>
                                                            <tr>
                                                                      <th>Judul Dakwah</th>
                                                                      <th>Tanggal Buat</th>
                                                                      <th>Aksi</th>

                                                            </tr>
                                                  </thead>
                                                  <tbody>
                                                            <?php foreach ($data_dakwah as $value): ?>
                                                                                <tr>
                                                                                          <td><?= $value->d_judul; ?></td>
                                                                                          <td><?= $value->d_pinaq; ?></td>
                                                                                          <td><?= anchor('muri/dakwah/kremaq/' . $value->d_id, '<i class="material-icons tiny">create</i>', ['title' => 'edit halaman']) ?> | 
                                                                                                    <?= anchor('muri/dakwah/sedaq/' . $value->d_id, '<i class="material-icons tiny red-text">delete_forever</i>', ['onclick' => 'return confirm(\'Apakah side yakin mele hapus dete sine.?\')', 'title' => 'hapus halaman'])
                                                                                                    ?>
                                                                                          </td>

                                                                                </tr>
                                                                      <?php endforeach; ?>

                                                  </tbody>
                                                  <tfoot>
                                                            <tr>
                                                                      <th>Judul Halaman</th>
                                                                      <th>Tanggal BUat</th>
                                                                      <th>Aksi</th>
                                                            </tr>
                                                  </tfoot>
                                        </table>

                    </div>
                    <div class="col m3 s12 out-vertical-10">
                              <h5 class="orange-text">Menu</h5>
                              <?php $this->load->view('muri/muri_sidebar'); ?>
                    </div>
          </div>
</div>

