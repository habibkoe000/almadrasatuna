<?php
          defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en" class="no-js">
          <head>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <title><?= $judul ?></title>
                    <!-- Tell the browser to be responsive to screen width -->
                    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                    <title><?= $judul ?></title>
                    <script src="<?= base_url(); ?>assets/js/modernizr.js"></script>
                    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/login.css" type="text/css">
                    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/materialize.min.css" type="text/css">
                    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                    <link rel="icon" href="<?= base_url(); ?>assets/img/icon.png">
          </head>
          <body>

                    <div class="container">
                              <div class="row out-top-150 box">
                                        <div class="col m8">
                                                  .
                                        </div>
                                        <div class="col m4 z-depth-2 white border-radius-3">
                                                  <div class="col s12">
                                                            <h4 class="judul">Al-Madrasatuna</h4>
                                                            <h6 class="sub-judul">Masuk</h6>
                                                  </div>
                                                  <p><?= validation_errors(); ?></p>
                                                  <?= form_open('teme'); ?>
                                                            <div class="row">
                                                                      <div class="input-field col s12">
                                                                                <input id="u_aran_user" type="text" name="u_aran_user" class="validate">
                                                                                <label for="u_aran_user">Nama User</label>
                                                                      </div>
                                                            </div>
                                                            <div class="row">
                                                                      <div class="input-field col s12">
                                                                                <input id="u_password" type="password" name="u_password" class="validate">
                                                                                <label for="u_password">Password</label>
                                                                      </div>
                                                            </div>
                                                            <div class="row">
                                                                      <div class="col s6">
                                                                                Anda belum daftar.?
                                                                                Silahkan <a href="<?= base_url().'teme/daftar' ?>">Daftar</a>
                                                                      </div>
                                                                      <div class="col s6">
                                                                                <button class="btn waves-effect waves-light right" type="submit" name="action">Masuk
                                                                                          <i class="material-icons right">send</i>
                                                                                </button>
                                                                      </div>
                                                            </div>

                                                  <?= form_close(); ?>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col s12">
                                                  &COPY; Al-Madrasatuna <?= date('Y') ?> by <a href="http://lombokinnovation.com" target="_blank">Lombok Innovation</a>
                                        </div>
                              </div>
                    </div>
                    
                    <!-- jQuery 2.1.4 -->
                    <script src="<?= base_url(); ?>assets/js/jquery.js"></script>
                    <!-- Bootstrap 3.3.5 -->
                    <script src="<?= base_url(); ?>assets/js/materialize.min.js"></script>
          </body>
</html>