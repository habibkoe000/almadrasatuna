<div class="container out-top-10 main">
          <div class="row">
                    <div class="col s12 nav-breadcumd">
                              <a href="<?= base_url() ?>">Dashboard</a> / <a href="">User</a> / <a href=""><?= $user_data->u_aran_lengkap; ?></a>
                    </div>
          </div>
          <div class="row">
                    <div class="col m9 s12 out-vertical-10">
                              <h5>User</h5>
                              <?= form_open_multipart('muri/user'); ?>
                              <div class="row">
                                        <div class="input-field col s6">
                                                  <input id="u_aran_lengkap" type="text" name="u_aran_lengkap" class="validate" value="<?= $user_data->u_aran_lengkap; ?>">
                                                  <label for="u_aran_lengkap">Nama Lengkap</label>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="input-field col s6">
                                                  <input id="u_kelamin" type="text" name="u_kelamin" class="validate" value="<?= $user_data->u_kelamin; ?>">
                                                  <label for="u_kelamin">Jenis Kelamin</label>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="input-field col s6">
                                                  <input id="u_email" type="text" name="u_email" class="validate" value="<?= $user_data->u_email; ?>">
                                                  <label for="u_email">Email User</label>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="input-field col s12">
                                                  <textarea id="u_alamat" class="materialize-textarea"><?= $user_data->u_alamat; ?></textarea>
                                                  <label for="u_alamat">Alamat</label>
                                        </div>
                            </div>
                              <div class="row">
                                        <div class="input-field col s6">
                                                  <input id="u_telepon" type="text" name="u_telepon" class="validate" value="<?= $user_data->u_telepon; ?>">
                                                  <label for="u_telepon">Handphone</label>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="input-field col s4">
                                                  <input id="u_tanggal_lahir" type="text" name="u_tanggal_lahir" class="validate" value="<?= $user_data->u_tanggal_lahir; ?>">
                                                  <label for="u_tanggal_lahir">Tanggal Lahir</label>
                                        </div>
                                        <div class="input-field col s4">
                                                  <input id="u_bulan_lahir" type="text" name="u_bulan_lahir" class="validate" value="<?= $user_data->u_bulan_lahir; ?>">
                                                  <label for="u_bulan_lahir">Bulan Lahir</label>
                                        </div>
                                        <div class="input-field col s4">
                                                  <input id="u_tahun_lahir" type="text" name="u_tahun_lahir" class="validate" value="<?= $user_data->u_tahun_lahir; ?>">
                                                  <label for="u_tahun_lahir">Tahun Lahir</label>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col s12">
                                                  <button class="btn waves-effect waves-light right" type="submit" name="action">Update
                                                            <i class="material-icons right">send</i>
                                                  </button>
                                        </div>
                              </div>
                              <?= form_close(); ?>
                    </div>
                    <div class="col m3 s12 out-vertical-10">
                              <div class="row">
                                        <div class="col s12">
                                                  <h5 class="orange-text">Menu</h5>
                                                  <?php $this->load->view('muri/muri_sidebar'); ?>
                                        </div>
                              </div>
                    </div>
          </div>
</div>

