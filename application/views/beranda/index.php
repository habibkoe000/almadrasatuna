<div id="index-banner" class="parallax-container">
          <div class="section no-pad-bot">
                    <div class="container">
                              <br><br>
                              <h1 class="header center blue-text text-lighten-2">Belajar Asik Dan Mudah</h1>
                              <div class="row center">
                                        <h5 class="header col s12 light">Belajar "Dimanapun", "Kapanpun" dan "Dengan Siapapun"</h5>
                              </div>
                              <div class="row center">
                                        <a href="<?= base_url().'teme'; ?>" id="masuk" class="btn-large waves-effect waves-light teal lighten-1">Silahkan Di coba</a>
                              </div>
                              <br><br>

                    </div>
          </div>
          <div class="parallax"><img src="<?= base_url(); ?>assets/img/background31.jpg" alt="Santri belajar sendiri"></div>
</div>


<div class="container">
          <div class="section">

                    <!--   Icon Section   -->
                    <div class="row">
                              <div class="col s12 m4">
                                        <div class="icon-block">
                                                  <h2 class="center green-text"><i class="material-icons">language</i></h2>
                                                  <h5 class="center"><a href="<?= base_url().'h/belajar-online'  ?>">Belajar Online</a></h5>

                                                  <p class="center light">
                                                            Fitur Belajar Online ini merupakan fitur utama dalam website ini, Siswa bisa belajar secara online melalui fasilitas ini, selain itu, siswa juga bisa menulis dakwah
                                                            sehingga bisa di baca oleh siswa yang lain.
                                                  </p>
                                        </div>
                              </div>
                              <div class="col s12 m4">
                                        <div class="icon-block">
                                                  <h2 class="center orange-text"><i class="material-icons">border_color</i></h2>
                                                  <h5 class="center"><a href="<?= base_url().'h/trayout-online'  ?>">Try Out Online</a></h5>

                                                  <p class="center light">
                                                            Fasilitas Try Out ini bisa di manfaatkan oleh siswa untuk melatih diri dalam menjawab contoh-contoh soal ujian nasional,
                                                            Soal yang tersedia di website ini merupakan soal yang di ambil dari ujian-ujian nasional sebelumnya
                                                  </p>
                                        </div>
                              </div>

                              <div class="col s12 m4">
                                        <div class="icon-block">
                                                  <h2 class="center blue-text"><i class="material-icons">spellcheck</i></h2>
                                                  <h5 class="center"><a href="<?= base_url().'h/quiz-online'  ?>">Quiz Online</a></h5>

                                                  <p class="center light">
                                                            Fasilitas Quiz bisa di manfaatkan oleh siswa untuk berlatih mengerjakan soal, soal yang kami sediakan tentu sesuai dengan 
                                                           mata pelajaran yang di ajarkan di Madrasah(MI, MTs, MA)
                                                  </p>
                                        </div>
                              </div>
                    </div>

          </div>
</div>


<div class="parallax-container valign-wrapper">
          <div class="section no-pad-bot">
                    <div class="container">
                              <div class="row center">
                                        <div class="slider-container" id="slider-container">
                                                  <div class="slider">
                                                            <div class="slide">
                                                                      <h5 class="orange-text">"Hidup di dunia hanya sementara, jadi mari kita manfaatkan sebaik mungkin untuk melakukan kebajikan, entah itu kebajikan sesama manusia, dan yang lainnya"</h5>
                                                            </div>
                                                            <div class="slide">
                                                                      <h5 class="orange-text">"Apakah engkau meremehkan suatu doa kepada Allah, apakah engkau tahu keajaiban dan kemukjizatan doa? Ibarat panah dimalam hari, ia tidak akan meleset namun ia punya batas dan setiap batas ada saatnya untuk selesai"</h5>
                                                            </div>
                                                            <div class="slide">
                                                                      <h5 class="orange-text">"Barang siapa yang bersungguh-sungguh berjalan pada jalannya maka pasti ia akan sampai pada tujuannya"</h5>
                                                            </div>
                                                            <div class="slide">
                                                                      <h5 class="orange-text">"Ilmu pengetahuan diwaktu kecil itu bagaikan ukiran diatas batu"</h5>
                                                            </div>
                                                            <div class="slide">
                                                                      <h5 class="orange-text">"Kegagalan adalah cara Allah untuk mengatakan bersabarlah karena aku memiliki sesuatu yang lebih baik untukmu saat waktunya tiba"</h5>
                                                            </div>

                                                  </div>

                                                  <div class="switch" id="prev"><span></span></div>
                                                  <div class="switch" id="next"><span></span></div>
                                        </div>
                                        
                              </div>
                    </div>
          </div>
          <div class="parallax"><img src="<?= base_url(); ?>assets/img/background7.jpg" alt="Phanorama Kartun"></div>
</div>

<div class="container" id="tentang-kami">
          <div class="section">

                    <div class="row">
                              <div class="col s12 center">
                                        <h2 class="center red-text"><img src="<?= base_url(); ?>assets/img/logo.png"></h2>
                                        <h4><a href="<?= base_url().'h/tentang-almadrasatuna'  ?>">Tentang Al-Madrasatuna</a></h4>
                                        <p class="center light">
                                                  Kami adalah penyedia layanan online untuk Madrasah, fasilitas yang kami sediakan di website ini berupa tes online, quiz online, belajar online
                                                  dimana materi dan soal akan di isi oleh guru yang bersedia dengan sukarela membagi ilmunya di website ini, kenapa kami memilih Madrasah(MI, MTS, MA)
                                                  alasan pertama adalah, karena Madrasah merupakan salah satu sarana yang baik bagi generasi islam untuk mendapatkan pendidikan yang sesuai syariat Islam, 
                                                  oleh karena itu kami akan mendukung  sekolah yang masuk kategori Madrasah(MI, MTS, MA)...(<a href="<?= base_url().'h/tentang-almadrasatuna'  ?>">Selengkapnya</a>)
                                        </p>
                              </div>
                    </div>

          </div>
</div>


<div class="parallax-container valign-wrapper">
          <div class="section no-pad-bot">
                    
                    <div class="container center">
                              <h4><a href="<?= base_url().'dakwah'  ?>">Dakwah</a></h4>
                              <div class="row">
                                        <?php foreach($dakwah as $value): ?>
                                                  <div class="col s12 m3">
                                                            <div class="icon-block">
                                                                      <p class="center info-white">
                                                                                <a href="<?= base_url().'dakwah/'.$value->d_url ?>">
                                                                                          <?= $value->d_judul ?>
                                                                                </a>
                                                                      </p>
                                                              </div>
                                                            
                                                  </div>
                                        <?php endforeach; ?>
                              </div>
                    </div>
          </div>
          <div class="parallax"><img src="<?= base_url(); ?>assets/img/background41.jpg" alt="Santriwati menghadiri acara di aula"></div>
</div>