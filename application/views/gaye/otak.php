<?php
          defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html class="no-js">
          <head>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <title><?= $judul ?></title>
                    <!-- Tell the browser to be responsive to screen width -->
                    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                    <script src="<?= base_url(); ?>assets/js/modernizr.js"></script>
                    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/materialize.min.css" type="text/css" media="screen,projection">
                    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/muri.css" type="text/css">
                    <!-- DataTables -->
                    <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
                    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                    <link rel="icon" href="<?= base_url(); ?>assets/img/icon.png">
          </head>
          <body>

                    <div class="navbar-fixed">
                              <nav class="teal">
                                        <div class="nav-wrapper container">
                                                  <a href="<?= base_url().'muri/dashboard' ?>" class="brand-logo"><img src="<?= base_url(); ?>assets/img/logo-2.png"></a>
                                                  <ul class="right hide-on-med-and-down">
                                                            
                                                            <li><a href="<?= base_url().'muri/dashboard' ?>"><i class="material-icons">home</i></a></li>
                                                            <li><a href="<?= base_url().'muri/user' ?>"><?= $this->session->userdata('u_aran_lengkap'); ?></a></li>
                                                            <li><a href="<?= base_url().'sugun' ?>" title="keluar"><i class="material-icons">power_settings_new</i></a></li>
                                                            
                                                  </ul>
                                                  <ul id="slide-out" class="side-nav">
                                                            <li><a href="<?= base_url().'muri/dashboard' ?>">Dashboard</a></li>
                                                            <li><a href="<?= base_url().'muri/user' ?>"><?= $this->session->userdata('u_aran_lengkap'); ?></a></li>
                                                            <li><a href="<?= base_url().'sugun' ?>" title="keluar"><i class="material-icons">power_settings_new</i></a></li>
                                                  </ul>
                                                  <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
                                        </div>
                              </nav>
                    </div>
