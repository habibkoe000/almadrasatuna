<footer class="page-footer teal">
          <div class="container">
                    <div class="row">
                              
                              <div class="col l3 s12">
                                        <h6 class="black-text">Fitur</h6>
                                        <ul>
                                                  <?php foreach ($layanan as $value): ?>
                                                  <li><a class="white-text" href="<?= base_url().'h/'.$value->h_url?>"><?= $value->h_judul ?></a></li>
                                                  <?php endforeach; ?>
                                        </ul>
                              </div>
                              <div class="col l3 s12">
                                        <h6 class="black-text">Info</h6>
                                        <ul>
                                                  <li><a class="white-text" href="<?= base_url().'h/tentang-almadrasatuna'  ?>">Tentang Al-Madrasatuna</a></li>
                                                  <li><a class="white-text" href="<?= base_url().'h/tentang-almadrasatuna'  ?>">Kritik Dan Saran</a></li>
                                                   <p>
                                                             <a href="https://www.facebook.com/almadrasatuna/" target="_blank"><img src="<?= base_url().'assets/img/fb.png' ?>"></a> 
                                                             <a href="https://www.twitter.com" target="_blank"><img src="<?= base_url().'assets/img/twitter.png' ?>"></a>  
                                                             <a href="https://plus.google.com" target="_blank"><img src="<?= base_url().'assets/img/g+.png' ?>"></a>
                                                             <a href="https://youtube.com" target="_blank"><img src="<?= base_url().'assets/img/youtube.png' ?>"></a>
                                                  </p>
                                                  
                                        </ul>
                              </div>
                              <div class="col l6 s12">
                                        <h6 class="black-text">Al-Madrasatuna</h6>
                                        <p class="grey-text text-lighten-4">Tujuan kami ada adalah untuk membuat fasilitas dan layanan yang mudah dan bermanfaat bagi generasi muda islam</p>


                              </div>
                             
                    </div>
          </div>
          <div class="footer-copyright">
                    <div class="container">
                              &COPY; Al-Madrasatuna <?= date('Y') ?> by <a class="brown-text text-lighten-3" href="http://lombokinnovation.com" target="_blank">Lombok Innovation</a>
                    </div>
          </div>
</footer>
<script src="<?= base_url(); ?>assets/js/jquery.js"></script>
<!-- Materialize -->
<script src="<?= base_url(); ?>assets/js/materialize.min.js"></script>
<script src="<?= base_url(); ?>assets/js/slider.js"></script>
<script src="<?= base_url(); ?>assets/js/init.js"></script>
<script>
          $("#slider-container").sliderUi({
                    speed: 500,
                    cssEasing: "cubic-bezier(0.285, 1.015, 0.165, 1.000)"
          });
          $("#caption-slide").sliderUi({
                    caption: true
          });
</script>
</body>
</html>
