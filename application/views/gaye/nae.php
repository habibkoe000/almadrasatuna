

<footer>
          <div class="footer-copyright">
                    <div class="container">
                              &COPY; Al-Madrasatuna <?= date('Y') ?> by <a class="brown-text text-lighten-3" href="http://lombokinnovation.com" target="_blank">Lombok Innovation</a>
                    </div>
          </div>
</footer>
<!-- jQuery 2.1.4 -->
<script src="<?= base_url(); ?>assets/js/jquery.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?= base_url(); ?>assets/js/materialize.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/tinymce/tinymce.min.js"></script>
<?php $this->load->view('gaye/editor'); ?>
<script>
          $(document).ready(function () {
                    $(".button-collapse").sideNav();
          });
</script>
<script>
          $(document).ready(function () {
                    $('select').material_select();
          });
</script>
<script>
          $(function () {
                    $("#example1").DataTable();
          });
</script>
</body>
</html>
