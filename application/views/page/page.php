<div class="container out-top-10 main">
          <div class="row">
                    <div class="col s12 nav-breadcumd">
                              <a href="<?= base_url() ?>">Beranda</a> / <a href=""><?= $data_detail->h_jenis ?></a> / <a href=""><?= $data_detail->h_judul ?></a>
                    </div>
          </div>
          <div class="row">
                    <div class="col m9 s12 out-vertical-10">
                              <h4 class="center"><?= $data_detail->h_judul ?></h4>
                              <?= $data_detail->h_isi ?>
                    </div>
                    <div class="col m3 s12">
                              <h5 class="orange-text">Info Lainnya</h5>
                              <ul class="collection">
                                        <?php foreach ($data_halaman as $value): ?>
                                        <li class="collection-item avatar">
                                                  <i class="material-icons circle teal">favorite</i>
                                                  <a href="<?= base_url().'h/'.$value->h_url ?>"><span class="title"><?= $value->h_judul ?></span></a>
                                        </li>
                                        <?php endforeach; ?>
                              </ul>
                    </div>
          </div>
</div>