<div class="container out-top-10 main">
          <div class="row">
                    <div class="col s12 nav-breadcumd">
                              <a href="<?= base_url() ?>">Dashboard</a> / <a href="<?= base_url().'muri/halaman' ?>">Halaman</a> / <a href="<?= base_url().'muri/halaman/kremaq' ?>">Edit Halaman</a>
                    </div>
          </div>
          <div class="row">
                    <?= form_open('muri/halaman/kremaq/'.$data_detail->h_id) ?>
                    <div class="col m9 s12 out-vertical-10">
                              <h5><?= $judul ?></h5>
                              
                              <div class="row">
                                        <div class="input-field col s12">
                                                  <input id="h_judul" type="text" name="h_judul" class="validate" value="<?= $data_detail->h_judul ?>">
                                                  <label for="h_judul">Judul Halaman</label>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col s12">
                                                  <textarea id="tinyMce" name="h_isi"><?= $data_detail->h_isi ?></textarea>
                                        </div>
                              </div>
                              

                    </div>
                    <div class="col m3 s12 out-vertical-10">
                              <div class="row">
                                        <div class="col s12">
                                                  <h6 class="orange-text bold">Terbitkan</h6>
                                                  <p>
                                                            Status : Draft <br>
                                                            Visibility : Umum <br>

                                                  </p>
                                                  <button class="btn waves-effect waves-light right" type="submit">Terbitkan
                                                  </button>
                                        </div>
                              </div>
                              <hr>
                              <div class="row">
                                        <div class="col s12">
                                                  <h6 class="orange-text bold">Kategori</h6>
                                                  <p>
                                                            <input name="h_jenis" type="radio" id="h_jenis" value="layanan" />
                                                            <label for="h_jenis">Layanan</label>
                                                  </p>
                                                  <p>
                                                            <input name="h_jenis" type="radio" id="h_jenis2" value="info" />
                                                            <label for="h_jenis2">Info</label>
                                                  </p>
                                                  
                                        </div>
                              </div>
                              <hr>
                              <div class="row">
                                        <div class="col s12">
                                                  <h6 class="orange-text">Menu</h6>
                                                  <?php $this->load->view('muri/muri_sidebar'); ?>
                                        </div>
                              </div>
                    </div>
                    <?= form_close() ?>
          </div>
</div>

