<div class="container out-top-10 main">
          <div class="section no-pad-bot" id="index-banner">
                    <div class="container">
                              <br>
                              <h3 class="center orange-text">Selamat Datang Para Siswa & Siswi</h2>
                                        <div class="row center">
                                                  <h5 class="header col s12 light">Assalamualaikum <?= $this->session->userdata('u_aran_lengkap'); ?></h5>
                                        </div>

                                        <br>

                                        </div>
                                        </div>
                                        <div class="row">
                                                  <div class="col m4 s6 out-vertical-10">
                                                            <a href="<?= base_url() . 'muri/belajar' ?>" class="btn waves-effect waves-light btn-home" >
                                                                      Belajar Online<br>
                                                                      <i class="large material-icons center">language</i>
                                                            </a>
                                                  </div>
                                                  <div class="col m4 s6 out-vertical-10">
                                                            <a href="<?= base_url() . 'muri/ujian' ?>" class="btn waves-effect waves-light btn-home" >
                                                                      Try Out Online<br>
                                                                      <i class="large material-icons center">border_color</i>
                                                            </a>
                                                  </div>
                                                  <div class="col m4 s6 out-vertical-10">
                                                            <a href="<?= base_url() . 'muri/ujian' ?>" class="btn waves-effect waves-light btn-home" >
                                                                      Quiz Online<br>
                                                                      <i class="large material-icons center">spellcheck</i>
                                                            </a>
                                                  </div>
                                                  <div class="col m4 s6 out-vertical-10">
                                                            <a href="<?= base_url() . 'muri/dakwah' ?>" class="btn waves-effect waves-light btn-home" >
                                                                      Dakwah<br>
                                                                      <i class="large material-icons center">record_voice_over</i>
                                                            </a>
                                                  </div>
                                        </div>
                    </div>