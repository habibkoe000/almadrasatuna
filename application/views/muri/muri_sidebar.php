<ul class="collection">
          <li class="collection-item avatar">
                    <i class="material-icons circle teal">home</i>
                    <a href="<?= base_url() . 'muri/dashboard' ?>"><span class="title">Dashboard</span></a>

          </li>
          <?php if($this->session->userdata('u_level')=='admin'): ?>
          <li class="collection-item avatar">
                    <i class="material-icons circle teal">devices</i>
                    <a href="<?= base_url() . 'muri/halaman/romboq' ?>"><span class="title">Konten Baru</span></a>

          </li>
          <?php endif; ?>
          <li class="collection-item avatar">
                    <i class="material-icons circle teal">record_voice_over</i>
                    <a href="<?= base_url() . 'muri/dakwah/romboq' ?>"><span class="title">Tulis Dakwah</span></a>
          </li>
          <li class="collection-item avatar">
                    <i class="material-icons circle teal">assignment</i>
                    <a href="<?= base_url() . 'muri/soal' ?>"><span class="title">Tulis Soal</span></a>
          </li>
          <li class="collection-item avatar">
                    <i class="material-icons circle teal">content_paste</i>
                    <a href="<?= base_url() . 'muri/ujian' ?>"><span class="title">Info Ujian</span></a>
          </li>
          <?php if($this->session->userdata('u_level')=='admin'): ?>
          <li class="collection-item avatar">
                    <i class="material-icons circle teal">supervisor_account</i>
                    <a href="<?= base_url() . 'muri/user/user_selapuq' ?>"><span class="title">Lihat User</span></a>
          </li>
          <?php endif; ?>
</ul>