<div class="container out-top-10 main">
          <div class="section no-pad-bot" id="index-banner">
                    <div class="container">
                              <br>
                              <h3 class="center orange-text">Selamat Datang Bapak / Ibu Guru</h2>
                                        <div class="row center">
                                                  <h5 class="header col s12 light">Assalamualaikum <?= $this->session->userdata('u_aran_lengkap'); ?></h5>
                                        </div>

                                        <br>

                                        </div>
                                        </div>
                                        <div class="row">
                                                  <div class="col m4 s6 out-vertical-10">
                                                            <a href="<?= base_url() . 'muri/soal' ?>" class="btn waves-effect waves-light btn-home" >
                                                                      Tulis Soal<br>
                                                                      <i class="large material-icons center">assignment</i>
                                                            </a>
                                                  </div>
                                                  <div class="col m4 s6 out-vertical-10">
                                                            <a href="<?= base_url() . 'muri/belajar' ?>" class="btn waves-effect waves-light btn-home" >
                                                                      Mengajar<br>
                                                                      <i class="large material-icons center">bookmark</i>
                                                            </a>
                                                  </div>
                                                  <div class="col m4 s6 out-vertical-10">
                                                            <a href="<?= base_url() . 'muri/kategori' ?>" class="btn waves-effect waves-light btn-home" >
                                                                      Kategori Soal<br>
                                                                      <i class="large material-icons center">bookmark</i>
                                                            </a>
                                                  </div>
                                                  <div class="col m4 s6 out-vertical-10">
                                                            <a href="<?= base_url() . 'muri/ujian' ?>" class="btn waves-effect waves-light btn-home" >
                                                                      Ujian<br>
                                                                      <i class="large material-icons center">bookmark</i>
                                                            </a>
                                                  </div>
                                                  <div class="col m4 s6 out-vertical-10">
                                                            <a href="<?= base_url() . 'muri/dakwah' ?>" class="btn waves-effect waves-light btn-home" >
                                                                      Dakwah<br>
                                                                      <i class="large material-icons center">record_voice_over</i>
                                                            </a>
                                                  </div>
                                        </div>
                    </div>