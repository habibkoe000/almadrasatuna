<?php
          defined('BASEPATH') OR exit('No direct script access allowed');
          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Teme
           *
           * @author Habib Koe Tkj
           */
          class Teme extends CI_Controller {

                    public function __construct() {
                              parent::__construct();
                              $this->load->model('teme_model');
                    }
                    /*
                     * Fungsi sine te manfaatan kedu te teme aning aplikasi
                     */
                    public function index() {
                              $this->form_validation->set_rules('u_aran_user', 'Aran User', 'trim|required');
                              $this->form_validation->set_rules('u_password', 'Password', 'trim|required|alpha_numeric');

                              if ($this->form_validation->run() == FALSE) {
                                        $file['judul'] = 'Masuk Ke Sistem';
                                        $this->load->view('teme', $file);
                              } else {
                                        //cek to database email and password
                                        $u_aran_user = set_value('u_aran_user');
                                        $u_password = set_value('u_password');
                                        $user_kenak = $this->teme_model->teme_proses($u_aran_user, $u_password);

                                        if ($user_kenak == TRUE) {
                                                  //login berhasil
                                                  $this->session->set_userdata('temeGati', TRUE);
                                                  $this->session->set_userdata('u_id', $user_kenak->u_id);
                                                  $this->session->set_userdata('u_aran_lengkap', $user_kenak->u_aran_lengkap);
                                                  $this->session->set_userdata('u_level', $user_kenak->u_level);
                                                  redirect('muri/dashboard');
                                        } else {
                                                  //username dan password salah
                                                  $this->session->set_flashdata('error', 'wrong username / password');
                                                  redirect('teme');
                                        }
                              }
                    }
                    /*
                     * Fungsi lek bawak sine kedu te bedaftar lek sistem
                     */
                    public function daftar() {
                              $this->form_validation->set_rules('u_aran_lengkap', 'Aran User', 'trim|required');
                              $this->form_validation->set_rules('u_aran_user', 'Password', 'trim|required|alpha_numeric');
                              $this->form_validation->set_rules('u_password', 'Aran User', 'trim|required');
                              $this->form_validation->set_rules('u_tanggal_lahir', 'Password', 'trim|required|alpha_numeric');
                              $this->form_validation->set_rules('u_bulan_lahir', 'Aran User', 'trim|required');
                              $this->form_validation->set_rules('u_tahun_lahir', 'Password', 'trim|required|alpha_numeric');

                              if ($this->form_validation->run() == FALSE) {
                                        $file['judul'] = 'Daftar Ke Sistem';
                                        $file['bulan'] = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                                        $this->load->view('daftar', $file);
                              } else {
                                        $daftar = $this->teme_model->daftar();
                                        if($daftar){
                                                  $file['judul'] = 'Selamat nggih, sukses side daftar';
                                                  $this->load->view('sukses', $file);
                                        }
                              }
                    }
          }
          