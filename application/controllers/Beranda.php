<?php
defined('BASEPATH') OR exit('No direct script access allowed');
          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Home
           *
           * @author Habib Koe Tkj
           */
          class Beranda extends CI_Controller{
                    
                    public function __construct() {
                              parent::__construct();
                              $this->load->model('beranda_model');
                    }
                    
                    /*
                     * fungsi index lek bawak sine, ye kedu te tampilan data lek halaman uteme
                     */
                    public function index(){
                              $file['judul'] = 'Belajar Dimanapun Kapanpun Dengan Siapapun | Al-Madrasatuna';
                              $file['layanan'] = $this->beranda_model->beit_halaman_kedu_jenis('layanan');
                              $file['dakwah'] = $this->beranda_model->beit_dakwah_kedu_jenis('dakwah',4);
                              $this->load->view('gaye/otak_julu', $file);
                              $this->load->view('beranda/index', $file);
                              $this->load->view('gaye/nae_julu', $file);
                    }
          }
          