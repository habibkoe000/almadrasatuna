<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Kategori
           *
           * @author Habib Koe Tkj
           */
          class Kategori extends CI_Controller{
                    
                    private $user_id;
                    private $user_level;
                    
                    public function __construct() {
                              parent::__construct();
                              $session = $this->session->userdata('temeGati');
                              $this->user_level = $this->session->userdata('u_level');
                              $this->user_id = $this->session->userdata('u_id');
                              if ($session == FALSE) {
                                        redirect('teme');
                                        exit();
                              }
                    }
                    
                    public function index(){
                              $file['judul'] = 'Kategori';
                              $this->load->view('gaye/otak', $file);
                              $this->load->view('kategori/index', $file);
                              $this->load->view('gaye/nae');
                    }
          }
          