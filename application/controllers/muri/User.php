<?php
defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of User
           *
           * @author Habib Koe Tkj
           */
          class User extends CI_Controller{
                    
                    private $user_id;
                    
                    public function __construct() {
                              parent::__construct();
                              $session = $this->session->userdata('temeGati');
                              $this->user_id = $this->session->userdata('u_id');
                              if ($session == FALSE) {
                                        redirect('teme');
                                        exit();
                              }
                              $this->load->model('user_model');
                    }
                    /*
                     * fungsi lek bawak sine berfungsi jeri taok te edit data sendiri
                     * jeri lamun arak user mele edit dete ne, beu lengan fungsi lek bawak sine
                     */
                    public function index(){
                              $file['judul'] = 'User Management';
                              $file['user_data'] = $this->user_model->beit_sbiji($this->user_id);
                              $this->form_validation->set_rules('u_aran_lengkap', 'Nama Lengkap', 'required');
                              $this->form_validation->set_rules('u_kelamin', 'Kelamin', 'required');
                              $this->form_validation->set_rules('u_email', 'Email', 'required');
                              $this->form_validation->set_rules('u_alamat', 'Alamat', 'required');
                              $this->form_validation->set_rules('u_telepon', 'Telepon', 'required');
                              $this->form_validation->set_rules('u_tanggal_lahir', 'Tanggal Lahir', 'required');
                              $this->form_validation->set_rules('u_bulan_lahir', 'Bulan Lahir', 'required');
                              $this->form_validation->set_rules('u_tahun_lahir', 'Tahun Lahir', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->load->view('gaye/otak', $file);
                                        $this->load->view('user/index', $file);
                                        $this->load->view('gaye/nae');
                              } else {
                                        $this->user_model->kremaq($this->user_id);
                                        redirect('user');
                              }
                    }
                    
                    /*
                     * gitaq dete selapuq user si arak lek database,
                     * admin doang tao nggitak ye ine, lamun level si lein je ndek ne kanggo
                     */
                    public function user_selapuq(){
                              $file['judul'] = 'Semua User';
                              $this->load->view('gaye/otak', $file);
                              $this->load->view('user/selapuq', $file);
                              $this->load->view('gaye/nae');
                    }
          }
          