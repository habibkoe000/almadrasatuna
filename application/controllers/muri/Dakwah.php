<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Dakwah
           *
           * @author Habib Koe Tkj
           */
          class Dakwah extends CI_Controller{
                    
                    private $user_id;
                    private $user_level;
                    
                    public function __construct() {
                              parent::__construct();
                              $session = $this->session->userdata('temeGati');
                              $this->user_level = $this->session->userdata('u_level');
                              $this->user_id = $this->session->userdata('u_id');
                              if ($session == FALSE) {
                                        redirect('teme');
                                        exit();
                              }
                              $this->load->model('dakwah_model');
                    }
                    
                    public function index(){
                              $file['judul'] = 'Dakwah';
                              $file['data_dakwah'] = $this->dakwah_model->beit_kedu_user($this->user_id);
                              $this->load->view('gaye/otak', $file);
                              $this->load->view('dakwah/index', $file);
                              $this->load->view('gaye/nae');
                    }

                    public function romboq(){
                              $file['judul'] = 'Siarkan Dakwah';
                              $this->form_validation->set_rules('d_judul', 'Judul Halaman', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->load->view('gaye/otak', $file);
                                        $this->load->view('dakwah/romboq', $file);
                                        $this->load->view('gaye/nae');
                              } else {
                                        $this->dakwah_model->romboq($this->user_id);
                                        redirect('muri/dakwah');
                              }
                    }
                    
                    public function kremaq($id){
                              $file['judul'] = 'Edit Dakwah';
                              $file['data_detail'] = $this->dakwah_model->beit_sbiji($id);
                              $this->form_validation->set_rules('d_judul', 'Judul Halaman', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->load->view('gaye/otak', $file);
                                        $this->load->view('dakwah/kremaq', $file);
                                        $this->load->view('gaye/nae');
                              } else {
                                        $this->dakwah_model->kremaq($this->user_id, $id);
                                        redirect('muri/dakwah');
                              }
                    }
                    public function sedaq($id) {
                              $this->dakwah_model->sedaq($id);
                              redirect('muri/dakwah');
                    }
          }
          