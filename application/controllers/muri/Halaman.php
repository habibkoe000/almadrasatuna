<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Halaman
           * romboq = post
           * beit = get
           * kremaq = put
           * sedaq delete
           * beit_sbiji = find_one
           * 
           * @author Habib Koe Tkj
           */
          class Halaman extends CI_Controller{
                    
                    private $user_id;
                    private $user_level;
                    
                    public function __construct() {
                              parent::__construct();
                              $session = $this->session->userdata('temeGati');
                              $this->user_level = $this->session->userdata('u_level');
                              $this->user_id = $this->session->userdata('u_id');
                              if ($session == FALSE && $this->user_level !='admin') {
                                        redirect('teme');
                                        exit();
                              }
                              $this->load->model('halaman_model');
                    }
                    
                    /*
                     * nampilin selapuk data konten halaman lek bagian administrator
                     */
                    public function index(){
                              $file['judul'] = 'Konten Halaman';
                              $file['data_halaman'] = $this->halaman_model->beit();
                              $this->load->view('gaye/otak', $file);
                              $this->load->view('page/index', $file);
                              $this->load->view('gaye/nae');
                    }
                    
                    /*
                     * fungsi lek bawak sine te kedu rombok konten halaman
                     */
                    public function romboq(){
                              $file['judul'] = 'Tambah Halaman';
                              $this->form_validation->set_rules('h_judul', 'Judul Halaman', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->load->view('gaye/otak', $file);
                                        $this->load->view('page/romboq', $file);
                                        $this->load->view('gaye/nae');
                              } else {
                                        $this->halaman_model->romboq($this->user_id);
                                        redirect('muri/halaman');
                              }
                    }
                    
                    /*
                     * fungsi lek bawak sine ye te kedu ngeremak data halaman
                     */
                    public function kremaq($id){
                              $file['judul'] = 'Edit Halaman';
                              $file['data_detail'] = $this->halaman_model->beit_sbiji($id);
                              $this->form_validation->set_rules('h_judul', 'Judul Halaman', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->load->view('gaye/otak', $file);
                                        $this->load->view('page/kremaq', $file);
                                        $this->load->view('gaye/nae');
                              } else {
                                        $this->halaman_model->kremaq($this->user_id, $id);
                                        redirect('muri/halaman');
                              }
                    }
                    
                    /*
                     * fungsi lek bawak sine, ye si te kedu hapus data halaman
                     */
                    public function sedaq($id) {
                              $this->halaman_model->sedaq($id);
                              redirect('muri/halaman');
                    }
          }
          