<?php
defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Dashboard
           *
           * @author Habib Koe Tkj
           */
          class Dashboard extends CI_Controller{
                    private $user_id;
                    private $user_level;
                    public function __construct() {
                              parent::__construct();
                              $session = $this->session->userdata('temeGati');
                              $this->user_id = $this->session->userdata('u_id');
                              $this->user_level = $this->session->userdata('u_level');
                              if ($session == FALSE) {
                                        redirect('teme');
                                        exit();
                              }
                    }
                    
                    /*
                     * fungsi lek bawak sine, ye si nampilin halaman perteme lek dashboard
                     */
                    public function index(){
                              $file['judul'] = 'Dashboard';
                              $this->load->view('gaye/otak', $file);
                              if($this->user_level=='admin'){
                                        $this->load->view('muri/muri_admin');
                              }else if($this->user_level=='guru'){
                                        $this->load->view('muri/muri_guru');
                              }else{
                                        $this->load->view('muri/muri_siswa');
                              }
                              $this->load->view('gaye/nae');
                    }
          }
          