<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Page
           *
           * @author Habib Koe Tkj
           */
          class Halaman extends CI_Controller{
                    
                    public function __construct() {
                              parent::__construct();
                              $this->load->model('beranda_model');
                              $this->load->model('halaman_model');
                    }
                    
                    /*
                     * fungsi halaman_detail sine, ye taok te gitak deskripsi halamn si ken te bukak
                     */
                    public function halaman_detail($url){
                              $file['data_detail'] = $this->halaman_model->beit_kedu_url($url);
                              $file['data_halaman'] = $this->halaman_model->beit();
                              $file['layanan'] = $this->beranda_model->beit_halaman_kedu_jenis('layanan');
                              $file['judul'] = $file['data_detail']->h_judul;
                              $this->load->view('gaye/otak_julu', $file);
                              $this->load->view('page/page', $file);
                              $this->load->view('gaye/nae_julu');
                    }
          }
          